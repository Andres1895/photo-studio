import type { Metadata } from "next";
import Image from "next/image";

import { Inter } from "next/font/google";
import { CustomeLabel } from "../../components";
import "./globals.css";
import gitlabDark from "../../public/gitlab-dark-.svg";
import instagramSvg from "../../public/instagram-icon-1.svg";
import linkedIn from "../../public/linkedin-icon-1.svg";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Andrés Maturano González",
  description: "Andres's portfolio",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <main>
          <header
            role="header"
            className=" flex flex-row justify-center bg-header h-10 w-screen items-center align-middle"
          >
            <div className="flex justify-end w-screen space-x-4 pr-2">
              <CustomeLabel text="About" href="#about" />
              <CustomeLabel text="Experience" href="#experience" />
              <CustomeLabel text="Education" href="#about" />
              <CustomeLabel text="Skills" href="#about" />
              <CustomeLabel text="Hobbies" href="#about" />
            </div>
          </header>
          {children}
          <div
            role="footer"
            className="bg-footer flex justify-center text-red-50"
          >
            <label className="text-gray-500 font-primaryFont text-sm">
              Andrés Maturano González
            </label>
          </div>
        </main>
      </body>
    </html>
  );
}
