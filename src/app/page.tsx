import { Avatar, Card } from "../../components";
import Image from "next/image";
import gitlabDark from "../../public/gitlab-dark-.svg";
import instagramSvg from "../../public/instagram-icon-1.svg";
import linkedIn from "../../public/linkedin-icon-1.svg";

export default function Home() {
  return (
    <main className="flex flex-col bg-body items-center">
      <div id="about">
        <Card />
      </div>
      <div>
        <label className="font-primaryFont text-titlePrimary text-lg mb-4">
          Do you want to se me on TV?
        </label>
        <iframe
          className="w-screen h-screen p-32 md:p-14 sm:p-9 xs:p-3"
          src="https://www.youtube.com/embed/PsoKnGhrseQ"
        ></iframe>
      </div>
    </main>
  );
}
