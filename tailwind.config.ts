import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        header: '#7DA3B2',
        body: '#D9E5EB',
        footer: '#e0e0e0',
        cardPrimary: '#ecf3fd',
        cardSecondary: 'white',
        titleSection: '#3c5b7f',
        titlePrimary: '#5e9ae0',
        titleSecondary: '#444444',
        primaryText: '#73797d'
       },
       fontFamily: {
        primaryFont: ['PrimaryFont', 'sans-serif'],
       },
       fontSize: {
        'sm': '12px',
        'base': '16px',
        'lg': '20px',
        'xl': '32px'
      },
      screens: {
        'xs': '250px',
        'sm': '576px',
        'md': '768px',
        'lg': '992px',
        'xl': '1200px',
      },
    },
  },
  plugins: [],
};
export default config;
