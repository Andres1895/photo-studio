import Image from "next/image";
import profileImage from "../public/images/me.png";

const Avatar: React.FC = () => {
  return (
    <div role="image">
      <Image
        style={{ borderRadius: "10rem", margin: "1rem", maxWidth: "400px" }}
        src={profileImage}
        alt="Picture profile"
      />
    </div>
  );
};

export default Avatar;
