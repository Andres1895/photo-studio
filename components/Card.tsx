import "./styles.css";
import { Avatar } from "../components";

const FlipCard = () => {
  return (
    <div className="card">
      <div className="content">
        <div className="front">
          <Avatar />
        </div>
        <div className="back">
          <label className=" flex justify-center text-center font-primaryFont text-base align-middle text-primaryText px-10">
            {`
          A FRONT-END DEVELOPER with 5+ years of experience focused on web and
          mobile apps. Throughout my career I have worked on projects ranging
          from business developments to the banking - financial sector. Always
          being part of a high-performance team and having the opportunity to
          lead certain teams in front-end decision-making.
          One of my passions is to take photos. I try to carry my camera every
          place I go. I never know when I will be faced with a moment I
          want to immortalize.`}
          </label>
        </div>
      </div>
    </div>
  );
};

export default FlipCard;
