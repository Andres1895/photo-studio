type CustomeLabel = {
  text: string;
  href: string;
};

const CustomeLabel: React.FC<CustomeLabel> = ({ text, href }) => {
  return (
    <div className="mr-4">
      <a href={href} className="flex flex-1 mr-4 font-primaryFont text-base cursor-pointer">
        {text}
      </a>
    </div>
  );
};

export default CustomeLabel;
